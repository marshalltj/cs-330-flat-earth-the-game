﻿using UnityEngine;
using System.Collections;

public class DoorEventCheck : MonoBehaviour {	
	
	private GameObject player;
	private PlayerInventory inv;
	private PlayerMovement move;
	
	public string reqItem;
	
	void Awake(){
		player = GameObject.FindGameObjectWithTag (Tags.player);
		inv = player.GetComponent<PlayerInventory> ();
		move = player.GetComponent<PlayerMovement> ();
	}
	
	void OnTriggerStay(Collider other){
		if (other.gameObject == player && Input.GetKeyDown (KeyCode.E)) 
		if (inv.haveItem (reqItem)) {
			move.displayText ("You use the " + reqItem + ".", "small");
			Destroy (gameObject);
		}
		else
			move.displayText ("You need the " + reqItem + ".", "small");
	}
}
