﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {
	private GameObject player;

	public Vector3 newPosition;

	void Awake(){
		player = GameObject.FindGameObjectWithTag (Tags.player);
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject == player)
			player.rigidbody.position = newPosition;
	}
}
