﻿using UnityEngine;
using System.Collections;

public class WallShade : MonoBehaviour {
	public string type;

	void Awake(){
		if (type.Equals("wall"))
			renderer.material.color = Color.blue;
		else if (type.Equals("tele"))
			renderer.material.color = new Color(0,0,0,0f);
	}
}
