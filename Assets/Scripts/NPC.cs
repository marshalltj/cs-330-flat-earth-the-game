﻿using UnityEngine;
using System.Collections;

public class NPC : MonoBehaviour {
	private GameObject player;
	private PlayerMovement move;
	private PlayerInventory inv;

	public string msg;

	void Awake(){
		player = GameObject.FindGameObjectWithTag (Tags.player);
		inv = player.GetComponent<PlayerInventory> ();
		move = player.GetComponent<PlayerMovement> ();
	}

	void OnTriggerExit(Collider other){
		if (other.gameObject == player && inv.actFinished)
				Application.Quit();
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject == player && inv.actFinished){
			gameObject.transform.position = new Vector3 (37f, 1.3f, 0.0f);
			move.displayText(msg, "large");
		}
	}
}
