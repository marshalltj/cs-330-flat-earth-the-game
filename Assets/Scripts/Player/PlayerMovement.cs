﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public int playerSpeed = 1;
	public bool inMenu = false;
	public bool inText = false;
	public GameObject playerCam;
	public GameObject invCam; 
	public GUISkin style;

	private bool showText;
	private bool showImg;
	private string boxText;
	private string textType = "";
	private Texture image;
	
	void Awake(){
		rigidbody.freezeRotation = true;
		invCam.SetActive(false);
		playerCam.SetActive (true);
		showText = false;
	}

	// Update is called once per frame
	void Update () {

		if (inMenu == false)
			playerNav ();
		else
			menuNav ();
	}

	void playerNav(){
		if (Input.GetKey ("a")) 
			rigidbody.position += Vector3.left * playerSpeed * Time.deltaTime;
		
		if (Input.GetKey ("d")) 
			rigidbody.position += Vector3.right * playerSpeed * Time.deltaTime;
		
		if (Input.GetKey ("w")) 
			rigidbody.position += Vector3.up * playerSpeed * Time.deltaTime;
		
		if (Input.GetKey ("s")) 
			rigidbody.position += Vector3.down * playerSpeed * Time.deltaTime;

		if (Input.GetKeyDown ("i")) {
			inMenu = true;
			playerCam.SetActive(false);
			invCam.SetActive(true);
		}
	}

	void menuNav(){
		if (inText && Input.GetKeyDown ("space")){
			if (!showImg){
				inMenu = false;
				showText = false;
				inText = false;
			}
			else{
				showImg = false;
				showText = true;
			}
		}
			
		else if (!inText){
			if( Input.GetKeyDown ("i")) {
				inMenu = false;
				playerCam.SetActive(true);
				invCam.SetActive(false);
			}
		}
	}

	void OnGUI(){
		GUI.skin = style;
		int width=0; int height=0; int x=0; int y=0;
		if (textType.Equals("small")){
			width = Screen.width * 1/3;
			height = Screen.height/5;
			x = width;
			y = Screen.height*3/4;
		}
		else if (textType.Equals("medium")){
			width = Screen.width /2;
			height = Screen.height/5;
			x = Screen.width/4;
			y = Screen.height*3/4;
		}

		else if (textType.Equals("large")){
			width = Screen.width*3/4;
			height = Screen.height*3/4 ;
			x = Screen.width/8;
			y = Screen.height/8;
		}
		if (showText)
			GUI.Box(new Rect(x, y, width, height), boxText);
		if (showImg)
			GUI.DrawTexture (new Rect (x, y, width, height), image);
	}

	public void displayText(string text, string type){
		inMenu = true;
		showText = true;
		inText = true;
		boxText = text + "\n\n(Press space to close)";
		textType = type;
	}

	public void displayImg(Texture img, string text, string type){
		inMenu = true;
		showImg = true;
		inText = true;
		textType = type;
		boxText = text + "\n\n(Press space to close)";
		image = img;
	}
}
