﻿using UnityEngine;
using System.Collections;

public class OpenContainer : MonoBehaviour {
	private GameObject player;
	private PlayerInventory inv;
	private PlayerMovement move;
	private bool opened = false;

	public bool givesItems;
	public GameObject content;
	public string itemName;
	public Texture img;
	public string displayStr;
	
	void Awake(){
		player = GameObject.FindGameObjectWithTag (Tags.player);
		inv = player.GetComponent<PlayerInventory> ();
		move = player.GetComponent<PlayerMovement> ();
	}
	
	void OnTriggerStay(Collider other){
		if (other.gameObject == player && Input.GetKeyDown(KeyCode.E)){
			if (givesItems){
				if (!opened){
					opened = true;
					Vector3 invPos = inv.invItemVector();
					inv.inventoryStr.Add(itemName);
					inv.inventory.Add (content.gameObject);
					move.displayText("The container had a " + itemName + "in it.\n You take it.", "medium");
					content.transform.position = invPos;
				}
				else
					move.displayText("It's empty now.", "small");
			}
			else{
				move.displayImg(img, displayStr, "large");
			}
		}
	}	
}

