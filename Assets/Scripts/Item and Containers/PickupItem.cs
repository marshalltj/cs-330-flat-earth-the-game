﻿using UnityEngine;
using System.Collections;

public class PickupItem : MonoBehaviour {
	private GameObject player;
	private PlayerInventory inv;
	private PlayerMovement move;

	public string itemName;
	public string pickupText;
	public string textSize;
	public GameObject shadow;
	
	void Awake(){
		player = GameObject.FindGameObjectWithTag (Tags.player);
		inv = player.GetComponent<PlayerInventory> ();
		move = player.GetComponent<PlayerMovement> ();
		shadow.SetActive (false);
	}

	void OnTriggerStay(Collider other){
		if (other.gameObject == player && Input.GetKeyDown(KeyCode.E)){
			Vector3 invPos = inv.invItemVector();
			inv.inventoryStr.Add(itemName);
			inv.inventory.Add(this.gameObject);
			move.displayText(pickupText, textSize);
			transform.position = invPos;
			transform.localEulerAngles = new Vector3(0,0,0);
		}
	}
}

