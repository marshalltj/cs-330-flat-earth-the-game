using UnityEngine;
using System.Collections;

public class PlayerInventory : MonoBehaviour {
	private GameObject player;
	private PlayerMovement move;
	private int selectionIndex = 0;
	private GameObject currentItem;
	private PickupItem currentItemChild;
	private bool showText;

	public ArrayList inventoryStr;
	public ArrayList inventory;
	public GUISkin style;
	public string equipped;  
	public bool actFinished; 

	void Awake(){
		inventoryStr = new ArrayList ();
		inventory = new ArrayList ();
		player = GameObject.FindGameObjectWithTag (Tags.player);
		move = player.GetComponent<PlayerMovement> ();
		equipped = "Nothing";
	}

	void Update(){
		if (move.inMenu && !move.inText && inventory.Count != 0) {
			currentItem = inventory[selectionIndex] as GameObject; 
			currentItemChild = currentItem.GetComponent<PickupItem>();
			currentItemChild.shadow.SetActive(true); 

			if (Input.GetKeyDown("d") && selectionIndex+1 < inventory.Count && !showText){
				currentItemChild.shadow.SetActive(false);
				selectionIndex += 1; 
			}

			if (Input.GetKeyDown("a") && selectionIndex-1 >= 0 && !showText){
				currentItemChild.shadow.SetActive(false);
				selectionIndex -= 1;
			}
		
			if (Input.GetKeyDown("e"))
			    showText = true;
			if (Input.GetKeyDown("space"))
				showText = false;
			if (Input.GetKeyDown("f")){
				if (!(equipped.Equals(inventoryStr[selectionIndex] as string)))
					equipped = (inventoryStr[selectionIndex] as string);
				else
					equipped = "Nothing";
			}
		}

		if (haveItem("map"))
			actFinished = true;

	}

	public bool haveItem(string item){
		for (int i=0; i < inventoryStr.Count; i++)
			if (item.Equals(inventoryStr[i]))
				return true;
		return false;
	}

	public Vector3 invItemVector(){
		return new Vector3 (-47 + (inventoryStr.Count), 2, 0);
	}

	void OnGUI(){
		GUI.skin = style;
		if (showText && move.inMenu)
			GUI.Box(new Rect(Screen.width /4, Screen.height/4, Screen.width /2, Screen.height/2), currentItemChild.pickupText + "\n\n(Press space to close)");
		else if (move.inMenu && !move.inText && inventory.Count != 0)
			GUI.Box(new Rect(Screen.width * 1/3, Screen.height*3/4, Screen.width * 1/3, Screen.height/5), (inventoryStr[selectionIndex] as string) + "\n\nCurrently Eqipped:\n" + equipped);
	}
}
