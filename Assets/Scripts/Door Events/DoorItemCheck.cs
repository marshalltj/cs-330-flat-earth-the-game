﻿using UnityEngine;
using System.Collections;

public class DoorItemCheck : MonoBehaviour {	

	private GameObject player;
	private PlayerInventory inv;
	private PlayerMovement move;
	
	public string reqItem;
	public string openString;
	public string lockedString;
	public string textSize;

	void Awake(){
		player = GameObject.FindGameObjectWithTag (Tags.player);
		inv = player.GetComponent<PlayerInventory> ();
		move = player.GetComponent<PlayerMovement> ();
	}

	void OnTriggerStay(Collider other){
		if (other.gameObject == player && Input.GetKeyDown (KeyCode.E)) 
				if (inv.equipped.Equals(reqItem)) {
					move.displayText (openString, textSize);
					Destroy (gameObject);
				}
				else
					move.displayText (lockedString, textSize);
	}
}
